#include <Windows.h>
#include <MMSystem.h>
#include <math.h>

#include "AMClickRecognizer.h"


#ifdef DEBUG_MESSAGE
#include <stdio.h>
#define DPRINTF(...) printf(__VA_ARGS__)
#else
#define DPRINTF(...)
#endif

#ifndef VECTOR3CROSS
#define VECTOR3CROSS(A, B, C) (	C[0] = A[1]*B[2] - A[2]*B[1], \
								C[1] = A[2]*B[0] - A[0]*B[2], \
								C[2] = A[0]*B[1] - A[1]*B[0])
#endif

AMClickRecognizer::AMClickRecognizer():
	m_PosUpdatedTime(0),
	m_RecognizedClick(false),
	m_RecognitionStatus(STATUS_START), 
	m_DeltaZ(0), 
	m_MinDeltaZ(0),
	m_StatusWentDownTime(0), 
	m_StatusWentUpTime(0)
{
	m_Pos[0] = 0;
	m_Pos[1] = 0;
	m_Pos[2] = 0;

	m_Rotation[0] = 1;	m_Rotation[3] = 0;	m_Rotation[6] = 0;
	m_Rotation[1] = 0;	m_Rotation[4] = 1;	m_Rotation[7] = 0;
	m_Rotation[2] = 0;	m_Rotation[5] = 0;	m_Rotation[8] = 1;
}

AMClickRecognizer::~AMClickRecognizer()
{
}


void AMClickRecognizer::Reset()
{
	m_RecognizedClick = false;
	m_PosUpdatedTime = 0;
	m_RecognitionStatus = STATUS_START;
	m_DeltaZ = m_MinDeltaZ = 0;
	m_StatusWentDownTime = m_StatusWentUpTime = 0;
}

void AMClickRecognizer::Update(const float* pos)
{
	m_RecognizedClick = false;

	unsigned long oldTime = m_PosUpdatedTime;
	unsigned long currTime = timeGetTime();
	float dt = (float)(currTime - oldTime);

	// apply rotation to input pos
	float rotatedPos[3];
	rotatedPos[0] = m_Rotation[0]*pos[0] + m_Rotation[3]*pos[1] + m_Rotation[6]*pos[2];
	rotatedPos[1] = m_Rotation[1]*pos[0] + m_Rotation[4]*pos[1] + m_Rotation[7]*pos[2];
	rotatedPos[2] = m_Rotation[2]*pos[0] + m_Rotation[5]*pos[1] + m_Rotation[8]*pos[2];

	// backup old position
	float oldPos[3];
	oldPos[0] = m_Pos[0];
	oldPos[1] = m_Pos[1];
	oldPos[2] = m_Pos[2];

	// store new position and make it current
	m_Pos[0] = rotatedPos[0];
	m_Pos[1] = rotatedPos[1];
	m_Pos[2] = rotatedPos[2];

	m_PosUpdatedTime = currTime;
	if(oldTime == 0 || currTime - oldTime > 1000) {
		m_RecognitionStatus = STATUS_START;
		return;
	}

	float old_dz = m_DeltaZ;
	m_DeltaZ = (m_Pos[2] - oldPos[2]) / dt * 100;
	DPRINTF("\t%f", m_DeltaZ);
	

	// check down slope
	switch(	m_RecognitionStatus)
	{
	case STATUS_START:
		DPRINTF("\tSTART");
		if(m_DeltaZ < -0.03) // SUBJECT TO TWEAK
		{
			m_RecognitionStatus = STATUS_DOWN;
			m_StatusWentDownTime = currTime;
			m_MinDeltaZ = m_DeltaZ;
			m_PosWhenWentDown[0] = m_Pos[0];
			m_PosWhenWentDown[1] = m_Pos[1];
			m_PosWhenWentDown[2] = m_Pos[2];
		}
		break;
	case STATUS_DOWN:
		DPRINTF("\tDOWN");
		if(m_DeltaZ > 0) // SUBJECT TO TWEAK
		{
			if(m_DeltaZ > 0.1) // too big change, probably noise
			{
				DPRINTF(" NOISE?");
			}
			else if(m_MinDeltaZ > -0.05) // not enough power: SUBJECT TO TWEAK
			{
				m_RecognitionStatus = STATUS_START;
				DPRINTF(" LP");
			}
			else if(m_MinDeltaZ < -0.35) // too much power, doubting noise: SUBJECT TO TWEAK
			{
				m_RecognitionStatus = STATUS_START;
				DPRINTF(" HP");
			}
			else
			{
				m_RecognitionStatus = STATUS_UP;
				m_StatusWentUpTime = currTime;

				// check if too short down time
				if(m_StatusWentUpTime - m_StatusWentDownTime < 100) // SUBJECT TO TWEAK (roughly 3 frames)
				{
					m_RecognitionStatus = STATUS_START;
					DPRINTF(" shortD");
				}
				else {
					// check if the major movement is not toward Z axis
					float v[3];
					// use oldPos instead of m_Pos as at this point the hand is already rising
					v[0] = oldPos[0] - m_PosWhenWentDown[0];
					v[1] = oldPos[1] - m_PosWhenWentDown[1];
					v[2] = oldPos[2] - m_PosWhenWentDown[2];
					float norm = sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
					v[0] /= norm;
					v[1] /= norm;
					v[2] /= norm;
					if(v[2] > -0.71) // out of 45 deg (cos)-0.71 / 40 deg -0.77 / 35 deg -0.82 / 30 deg 0.86
					{
						
						// be generous to tending toward down (-Y)
						if(v[2] < -0.5 // at least in 60 degree (cos) -0.5 / 50 deg -0.64 / 45 deg -0.71
							&& v[1] < -0.64 // yet the major direction is in -Y (sin) 45deg -0.71 40deg -0.64
							)
						{
							// do nothing
						}
						else
						{
							m_RecognitionStatus = STATUS_START;
							DPRINTF(" ~Z %.2f %.2f %.2f", v[0], v[1], v[2]);
						}
					}
				}
			}
		}
		else
		{
			if(m_DeltaZ < m_MinDeltaZ)
				m_MinDeltaZ = m_DeltaZ;
		}
		break;
	case STATUS_UP:
		DPRINTF("\tUP");
		{
			unsigned long upTime = currTime - m_StatusWentUpTime;
			if(upTime > 100) // SUBJECT TO TWEAK (roughly 3 frames)
			{
				m_RecognizedClick = true;
				m_RecognitionStatus = STATUS_START;
			} 
			else
			{
				if(m_DeltaZ < -0.01) // SUBJECT TO TWEAK (tolerating noise slightly below zero)
				{
					m_RecognitionStatus = STATUS_START;
					DPRINTF(" shortU");
				}
			}
		}
		break;
	default:
		DPRINTF("\tDEFAULT");
		m_RecognitionStatus = STATUS_START;
	}
}

bool AMClickRecognizer::RecognizedClick()
{
	return m_RecognizedClick;
}

void AMClickRecognizer::SetGravityVector(const float* vec)
{
	float x[3], y[3], z[3];

	y[0] = -vec[0];
	y[1] = -vec[1];
	y[2] = -vec[2];

	float n = sqrt(y[0]*y[0] + y[1]*y[1] + y[2]*y[2]);
	y[0] /=n;
	y[1] /=n;
	y[2] /=n;

	x[0] = 1;
	x[1] = 0;
	x[2] = 0;

	VECTOR3CROSS(x, y, z);
	VECTOR3CROSS(y, z, x);

	m_Rotation[0] = x[0];	m_Rotation[3] = x[1];	m_Rotation[6] = x[2];
	m_Rotation[1] = y[0];	m_Rotation[4] = y[1];	m_Rotation[7] = y[2];
	m_Rotation[2] = z[0];	m_Rotation[5] = z[1];	m_Rotation[8] = z[2];

	DPRINTF("%.3f\t%.3f\t%.3f\n%.3f\t%.3f\t%.3f\n%.3f\t%.3f\t%.3f\n", 
		m_Rotation[0], m_Rotation[3], m_Rotation[6], 
		m_Rotation[1], m_Rotation[4], m_Rotation[7], 
		m_Rotation[2], m_Rotation[5], m_Rotation[8]);

	Reset();
}

void AMClickRecognizer::SetRotationMatrix(const float* mat)
{
	for(int i = 0; i < 9; i++)
		m_Rotation[i] = mat[i];

	Reset();
}