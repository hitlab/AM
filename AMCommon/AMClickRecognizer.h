// AMClickRecognizer.h

#pragma once

class AMClickRecognizer
{
protected:
	float m_Pos[3];
	unsigned long m_PosUpdatedTime;
	bool m_RecognizedClick;
	int m_RecognitionStatus;

	enum {STATUS_START = 0, STATUS_DOWN, STATUS_UP};
	float m_DeltaZ;
	float m_MinDeltaZ;
	unsigned long m_StatusWentDownTime;
	unsigned long m_StatusWentUpTime;

	// for making sure major movement is in Z axis
	float m_PosWhenWentDown[3];

	// matrix in column major for rotating point space
	float m_Rotation[9];

public:
	AMClickRecognizer();
	~AMClickRecognizer();

	void Reset();
	void Update(const float* pos);
	bool RecognizedClick();

	// float[3] gravity vector from kinect, eventually sets m_Rotation as the SetRotationMatrix
	void SetGravityVector(const float* vec);

	// float[9] in column major
	void SetRotationMatrix(const float* mat);
};
