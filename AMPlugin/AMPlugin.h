#pragma once

//-----------------------------------
#ifdef AM_PLUGIN
#define AM_API extern "C" __declspec(dllexport)
#else
#define AM_API extern "C" __declspec(dllimport)
#endif
//-----------------------------------

// the order and values are equivalent to Kinect SDK skeleton enumeration
enum _AM_SKELETON_INDEX {	
		AM_SKELETON_HIP_CENTER = 0,
		AM_SKELETON_SPINE,
		AM_SKELETON_SHOULDER_CENTER,
		AM_SKELETON_HEAD,
		AM_SKELETON_SHOULDER_LEFT,
		AM_SKELETON_ELBOW_LEFT,
		AM_SKELETON_WRIST_LEFT,
		AM_SKELETON_HAND_LEFT,
		AM_SKELETON_SHOULDER_RIGHT,
		AM_SKELETON_ELBOW_RIGHT,
		AM_SKELETON_WRIST_RIGHT,
		AM_SKELETON_HAND_RIGHT,
		AM_SKELETON_HIP_LEFT,
		AM_SKELETON_KNEE_LEFT,
		AM_SKELETON_ANKLE_LEFT,
		AM_SKELETON_FOOT_LEFT,
		AM_SKELETON_HIP_RIGHT,
		AM_SKELETON_KNEE_RIGHT,
		AM_SKELETON_ANKLE_RIGHT,
		AM_SKELETON_FOOT_RIGHT,
		AM_SKELETON_COUNT
    } 	AM_SKELETON_INDEX;


//DEFINE INTERFACE METHODS

//Nui Context Management

/// <summary>
/// Initialze 
/// </summary>
/// <returns>true on success, false otherwise</returns>
AM_API bool AMInitialize(int colourImageWidth);

/// <summary>
/// UnInitialze 
/// </summary>
AM_API void AMUnInitialize();

/// <summary>
/// Update needs to be call every frame
/// </summary>
AM_API bool AMUpdate();

/// <summary>
/// Update needs to be call every frame if using colour image
/// </summary>
AM_API bool AMUpdateColourImage();

/// <summary>
/// Get skeleton position
/// </summary>
/// <param name="index">index of the skeleton, same as kinect</param>
/// <param name="pos">pointer to float[3] to get ouput</param>
/// <returns>true if successful, false otherwise</returns>
AM_API bool AMGetSkeletonPos(IN int index, OUT float* pos);

/// <summary>
/// Get skeleton position in image coordinate
/// </summary>
/// <param name="index">index of the skeleton, same as kinect</param>
/// <param name="pos">pointer to float[3] to get ouput</param>
/// <returns>true if successful, false otherwise</returns>
AM_API bool AMGetSkeletonColourImagePos(IN int index, OUT float* pos);

/// <summary>
/// Get skeleton position in colour image
/// </summary>
/// <param name="index">index of the skeleton, same as kinect</param>
/// <param name="pos">pointer to float[2] to get ouput</param>
/// <returns>true if successful, false otherwise</returns>
AM_API bool AMGetSkeletonColourImagePos(IN int index, OUT float* pos);

/// <summary>
/// Check if right hand click happend in this frame.
/// Should be called after AMUpdate()
/// </summary>
/// <returns>true if clicked, false otherwise</returns>
AM_API bool AMRightHandClicked();


/// <summary>
/// Check if left hand click happend in this frame.
/// Should be called after AMUpdate()
/// </summary>
/// <returns>true if clicked, false otherwise</returns>
AM_API bool AMLeftHandClicked();

/*
/// <summary>
/// Set projection matrix for mirror visualization (multiplied to the raw hand position)
/// Initially, it is set to identity matrix, which will result in orthographic projection.
/// </summary>
/// <param name="proj">projection matrix float[12] in row major</param>
AM_API void AMSetProjection(const float* proj);

/// <summary>
/// Get projected 2D point of the given 3D point
/// </summary>
/// <param name="pos3">input position in float[3]</param>
/// <param name="pos2">output position in float[2]</param>
AM_API void AMGetProjectedPoint(const float* pos3, OUT float* pos2);
*/


AM_API bool AMGetGravityVector(OUT float* vec);

/// <summary>
/// Get colour image in byte array
/// </summary>
/// <returns>image in byte array, null if error</returns>
/// <param name="size">output size of the array</param>
AM_API BYTE* AMGetColourImage(OUT int* size);