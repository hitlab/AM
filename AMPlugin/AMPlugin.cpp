// AMPlugin.cpp : Defines the exported functions for the DLL application.
//

#include "WinCommon.h"
#include "AMPlugin.h"
#include "../AMCommon/AMKinectContext.h"
#include "../AMCommon/AMClickRecognizer.h"


AMClickRecognizer g_LeftHandClickRecognizer;
AMClickRecognizer g_RightHandClickRecognizer;

AM_API bool AMInitialize(int colourImageWidth)
{
	HRESULT hr = g_defaultAMKinectContext.Setup(colourImageWidth);

	if(FAILED(hr))
		return false;

	float gravity[3];
	while(!AMGetGravityVector(gravity));
	{
		g_LeftHandClickRecognizer.SetGravityVector(gravity);
		g_RightHandClickRecognizer.SetGravityVector(gravity);
	}

	return true;
}

AM_API void AMUnInitialize()
{
	g_defaultAMKinectContext.Cleanup();
}

bool skeletonUpToDate = false;

AM_API bool AMUpdate()
{
	bool result = SUCCEEDED(g_defaultAMKinectContext.ProcessSkeletonFrame());

	// if new skeleton tracking info available, update click recognizers
	if(result) {
		float rightHandPos[3];
		AMGetSkeletonPos(NUI_SKELETON_POSITION_WRIST_RIGHT, rightHandPos);
		g_RightHandClickRecognizer.Update(rightHandPos);

		float leftHandPos[3];
		AMGetSkeletonPos(NUI_SKELETON_POSITION_WRIST_LEFT, leftHandPos);
		g_LeftHandClickRecognizer.Update(leftHandPos);
	}

	skeletonUpToDate = result;

	return result;
}

AM_API bool AMUpdateColourImage()
{
	return SUCCEEDED(g_defaultAMKinectContext.ProcessColourFrame());
}

AM_API bool AMUpdateDepthImage()
{
	return SUCCEEDED(g_defaultAMKinectContext.ProcessDepthFrame());
}

AM_API void AMUpdateFusionImage()
{
	return g_defaultAMKinectContext.ProcessDepth();
}

AM_API bool AMGetSkeletonPos(IN int index, OUT float* pos)
{
	Vector4 skeletonPos = g_defaultAMKinectContext.m_SkeletonData.SkeletonPositions[index];
	pos[0] = skeletonPos.x / skeletonPos.w;
	pos[1] = skeletonPos.y / skeletonPos.w;
	pos[2] = skeletonPos.z / skeletonPos.w;

	return skeletonUpToDate;
}

AM_API bool AMGetSkeletonColourImagePos(IN int index, OUT float* pos)
{
	if(!skeletonUpToDate)
		return false;

	Vector4 skeletonPos = g_defaultAMKinectContext.m_SkeletonData.SkeletonPositions[index];
	
	LONG lDepthX, lDepthY, lColourX, lColourY;
	USHORT usDepthValue;

	NuiTransformSkeletonToDepthImage(skeletonPos, &lDepthX, &lDepthY, &usDepthValue, NUI_IMAGE_RESOLUTION_640x480); // depth resolution
	HRESULT hr = NuiImageGetColorPixelCoordinatesFromDepthPixelAtResolution(g_defaultAMKinectContext.m_colorImageResolution, 
																NUI_IMAGE_RESOLUTION_640x480, // depth resolution
																nullptr,
																lDepthX, lDepthY, usDepthValue,
																&lColourX, &lColourY);

	if(FAILED(hr))
		return false;

	pos[0] = (float)lColourX;
	pos[1] = (float)lColourY;	

	return true;
}

AM_API bool AMRightHandClicked()
{
	return g_RightHandClickRecognizer.RecognizedClick();
}

AM_API bool AMLeftHandClicked()
{
	return g_LeftHandClickRecognizer.RecognizedClick();
}

AM_API bool AMGetGravityVector(OUT float* vec)
{
	if(SUCCEEDED(g_defaultAMKinectContext.GetGravityVector(vec)))
		return true;
	
	return false;
}

AM_API byte* AMGetColourImage(OUT int* size)
{
	*size = g_defaultAMKinectContext.m_ColourImageSize;
	return g_defaultAMKinectContext.m_ColourImage;
}

AM_API byte* AMGetDepthImage(OUT int* size)
{
	*size = g_defaultAMKinectContext.m_DepthImageSize;
	return g_defaultAMKinectContext.m_DepthImage;
}

AM_API byte* AMGetFusionImage(OUT int* size)
{
	*size = g_defaultAMKinectContext.m_FusionImageSize;
	return g_defaultAMKinectContext.m_FusionImage;
}
